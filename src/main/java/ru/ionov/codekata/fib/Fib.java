package ru.ionov.codekata.fib;

import java.util.stream.Stream;

public class Fib {

    public int recursionMethod(int n){
        if(n == 2){
            return 1;
        }else if(n == 1){
            return 0;
        }
        return recursionMethod(n - 1) + recursionMethod(n - 2);
    }

    public int arrayMethod(int n){
        if(n <= 2)
            return n == 1 ? 0 : 1;
        int[] array = new int[n];
        array[0] = 0;
        array[1] = 1;
        for(int i = 2; i < array.length; i++){
            array[i] = array[i-1] + array[i-2];
        }
        return array[n-1];
    }

    public int optimizationMethod(int n){
        if(n == 1)
            return 0;
        if(n == 2)
            return 1;
        int current = 2;
        int temp = 0;
        int first = 0;
        int second = 1;
        while(current < n) {
            temp = first + second;
            first = second;
            second = temp;
            current++;
        }
        return temp;
    }

    public int streamMethod(int n){
        if(n <= 2)
            return n == 1 ? 0 : 1;
        final int[] first = {0};
        final int[] second = {1};
        int result = Stream.iterate(2, current -> {
                int answer = first[0] + second[0];
                first[0] = second[0];
                second[0] = answer;
                return answer;
            }).limit(n-1).skip(n-2).findAny().get();
        return result;
    }

}
