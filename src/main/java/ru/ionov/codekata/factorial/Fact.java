package ru.ionov.codekata.factorial;

import java.math.BigInteger;

public class Fact {

    private int a;

    public static void main(String[] args) {
        Fact factTest = new Fact();
        factTest.a++;
        System.out.println(factTest.a);
    }

    public long primitveFact(int n){
        if(n == 0 || n == 1)
            return 1;
        return n * primitveFact(n-1);
    }

    public BigInteger lineFact(int n){
        if(n <= 1)
            return BigInteger.valueOf(1);
        int zero = 1;
        int one = 1;
        BigInteger result = BigInteger.valueOf(zero * one);
        for(int i = 2; i <= n; i++){
            result = result.multiply(BigInteger.valueOf(i));
        }
        System.out.println(result.toString());
        return result;
    }


}
