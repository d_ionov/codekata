package ru.ionov.codekata.fib;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FibTest {

    private static Fib fib = new Fib();

    private final static int number = 10;

    private final static int standard = fib.recursionMethod(number);

    @Test
    public void recursionMethodTest(){
        int result = fib.recursionMethod(number);
        assertEquals(standard, result);
    }

    @Test
    public void arrayMethodTest(){
        int result = fib.arrayMethod(number);
        assertEquals(standard, result);
    }

    @Test
    public void optimMethodTest(){
        int result = fib.optimizationMethod(number);
        assertEquals(standard, result);
    }

    @Test
    public void streamMethodTest(){
        int result = fib.streamMethod(number);
        assertEquals(standard, result);
    }




}