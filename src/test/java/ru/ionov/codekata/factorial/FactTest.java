package ru.ionov.codekata.factorial;

import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class FactTest {

    private final Fact fact = new Fact();

    private static final int bigNumber = 600_000;

    private static final int smallNumber = 5;


    @Test
    public void testPrimiriveFact(){
        long result = fact.primitveFact(smallNumber);
        System.out.println(result);
        assertEquals(120, result);
    }

    @Test
    public void testArrayFact(){
        BigInteger result = fact.lineFact(bigNumber);
        System.out.println(result);
    }

}